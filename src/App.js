import React from 'react';
import { Fragment } from 'react';
import {
	Collapse,
	Navbar,
	NavbarToggler,
	NavbarBrand,
	Nav,
	NavItem,
	NavLink,
	UncontrolledDropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem,
	Container,
	Row,
	Col,
} from 'reactstrap';
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Redirect
} from "react-router-dom";
import SkyLight from 'react-skylight';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppLink from './AppLink';
import SysPage from './SysPage';
import AppPage from './AppPage';
import AppLogin from './AppLogin';
import AppRegister from './AppRegister'
import AppPass from './AppPass';
//import config from './config';

const TITLE = 'ListTask'

export default class App extends React.Component {
	constructor(props) {
		super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
			isOpen: false,
			isVisible: true,
			options: [],
			link: "",
			data: "",
			isVisReg: false,
			isVisPass: false,
			backend: 'http://listtask.local',
			menus: []
		};
		this.route = "";
		this.enterSystem = this.enterSystem.bind(this);
		this.viewRegister = this.viewRegister.bind(this);
		this.hideRegister = this.hideRegister.bind(this);
		this.viewPassword = this.viewPassword.bind(this);
		this.setPage = this.setPage.bind(this);
	}
	
	//Carga los elementos basicos del menú
	componentDidMount() {
		document.title = TITLE;
		fetch(window.location.protocol + "//" + window.location.hostname + "/outFunctions/menu")
		.then(res => res.json())
		.then(
			(result) => {
				this.setState({
					options: result.data
				});
		},
		// Note: it's important to handle errors here
		// instead of a catch() block so that we don't swallow
		// exceptions from actual bugs in components.
		(error) => {
			this.setState({
				isLoaded: true,
				error
			});
		})
	}

	toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
	}

	setPage(route) {
		console.log(route);
		if ((route !== undefined) && (route !== "")) {
			this.setState({
				link: route
			})
			fetch(window.location.protocol + "//" + window.location.hostname + "/functions/" + route, {
				headers:{
					'jwt': this.state.jwt
				}
			})
			.then(res => res.json())
			.then(
				(result) => {
					this.setState({
						link: this.link,
						isLink: this.state.link,
						data: result.data
					});
					//return result.data;
					//this.render();
				},
				// Note: it's important to handle errors here
				// instead of a catch() block so that we don't swallow
				// exceptions from actual bugs in components.
				(error) => {
					this.setState({
						isLoaded: true,
						error
					});
				}
			)
		} else {
			this.setState({data: [{str_name: "", data: route}]});
		}
	}
	//Llama a AppPage para llamar y renderizar este componente
	SendPage = (link) => {
		return <AppPage route={link}></AppPage>
	}

	enterSystem(value) {
		if (value.state.hasOwnProperty("menu")) {
			this.setState({
				isVisible: false,
				jwt: value.state.jwt,
				menu: value.state.menu,
				dictionary: value.state.dictionary
			});
		} else {
			alert('Usuario inalido');
		}
	}

	viewRegister() {
		this.setState({
			isVisReg: !this.state.isVisReg,
			isVisPass: false
		});
		this.simpleDialog.show();
	}

	hideRegister() {
		this.setState({
			isVisReg: false,
			isVisPass: false
		});
		this.simpleDialog.hide();
	}

	viewPassword() {
		this.setState({
			isVisPass: !this.state.isVisPass,
			isVisReg: false
		});
		this.simpleDialog.show();
	}

	render() {
		const style = this.state.isVisible ? {display: 'block'} : {display: 'none'};
		var formDialog = {
			width: '50%',
			'min-height': '50px',
			marginTop: '-300px',
			marginLeft: '-35%',
		};
		let viewOption = this.state.isVisReg;
		if (this.state.isVisReg) {
			viewOption = <AppRegister api={this.state.backend} close={this.hideRegister}></AppRegister>
		} else {
			viewOption = <AppPass></AppPass>
		}
		if (this.state.isVisible === true) {
			return <div style={style}>
				<Fragment >
					<Router>
						<Navbar color = "info" dark expand = "md">
							<NavbarBrand href="/">ListTask</NavbarBrand>
							<NavbarToggler onClick={this.toggle} />
							<Collapse navbar>
								<Nav navbar>
									{this.state.options.map(link => {
										return (
											<NavItem key={link.big_id.toString()}>
												<NavLink href={"/" + link.str_link}> 
													{link.str_link}
												</NavLink>
											</NavItem>
										);
									})}
								</Nav>
							</Collapse>
						</Navbar>
						<br />
						<Container fluid={true}>
							<Row>
								<Col>
									<Switch>
										{this.state.options.map(link => {
											return (
												<Route key={link.big_id.toString()} path={"/" + link.str_link} component={() => <AppLink link={link.str_link} />}>
												</Route>
											);
										})}
										<Route exact path="/" render={() => (<Redirect to="/Inicio"/>)}/>
									</Switch>
								</Col>
								<Col sm="3">
									<AppLogin enterSystem={this.enterSystem} viewRegister={this.viewRegister} viewPassword={this.viewPassword} api={this.state.backend}></AppLogin>
								</Col>
								<SkyLight dialogStyles={formDialog} hideOnOverlayClicked ref={ref => this.simpleDialog = ref} >
									{viewOption}
								</SkyLight>
							</Row>
						</Container>
					</Router>
				</Fragment>
			</div>
		} else {
			return <div>
				<Fragment >
					<Router>
						<Navbar color = "info" light expand = "md">
							<NavbarBrand href="/">Menu</NavbarBrand>
							<NavbarToggler onClick={this.toggle} />
							<Collapse navbar>
								<Nav navbar>
									{this.state.menu.menus.map(link => {
										return (
											<UncontrolledDropdown nav key = {link.str_menu}>
												<DropdownToggle nav caret className="text-light" color="ligth">
													{link.str_menu}
												</DropdownToggle>
												<DropdownMenu  className="alert-secondary">
												{link.str_submenu.map(submenu => {
														return (
															<DropdownItem  className="alert-secondary" key={submenu.str_action} onClick={this.setPage.bind(this, submenu.str_action)}>
																{submenu.str_menu}
															</DropdownItem>
														)
													}) 
												}
												</DropdownMenu>
											</UncontrolledDropdown>
										);
									})}
								</Nav>
							</Collapse>
						</Navbar>
						<br />
						<Container fluid={true}>
							<Row>
								<Col>
									<SysPage data={this.state.data} route={this.state.isLink} dictionary={this.state.dictionary} jwt={this.state.jwt}></SysPage>
								</Col>
							</Row>
						</Container>
					</Router>
				</Fragment>
			</div>
		}
	}
}