import React from 'react';
import { 
	Button, 
	Form, 
	FormGroup, 
	Label, 
	Input,
	Card,
	CardTitle
} from 'reactstrap';

export default class AppLogin extends React.Component {
    constructor(props) {
		super(props);
		this.state = {
			user: "",
			pass: "",
			viewRegister: props.viewRegister,
			viewPassword: props.viewPassword
		};
		this.handleChangeUser = this.handleChangeUser.bind(this);
		this.handleChangePass = this.handleChangePass.bind(this);
	}

	mySubmitHandler = (event) => {
		event.preventDefault();
		var message = this.state;
		fetch(this.props.api + "/users/", {
			method: "OPTIONS",
			mode: "cors", 
			cache: "no-cache", 
			credentials: "same-origin", 
			headers: {
				"Content-Type": "application/json"
			},
			redirect: "follow", 
			referrerPolicy: "no-referrer",
			body: JSON.stringify(message)
		})
		.then(res => res.json())
		.then(
			(result) => {
				if (result.access !== "") {
					this.setState({
						jwt: result.jwt,
						refresh: result.data,
						menu: result.menu,
						dictionary: result.dictionary
					});
					this.props.enterSystem(this, false);
				};
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
			alert('Usuario invalido');
        })
	}

	handleChangeUser(event) {
		this.setState({user: event.target.value});
	}

	handleChangePass(event) {
		this.setState({pass: event.target.value});
	}

	render() {
		return (
			<Card body>
				<CardTitle><h3>Acceso al Sistema</h3></CardTitle>
				<Form>
					<FormGroup>
						<Label for="user">Usuario</Label>
						<Input type="text" name="user" id="user" value={this.state.user}  onChange={this.handleChangeUser} placeholder="Introduzca su usuario" />
					</FormGroup>
					<FormGroup>
						<Label for="pass">Password</Label>
						<Input type="password" name="pass" id="pass" value={this.state.pass} onChange={this.handleChangePass} placeholder="Introduzca su password" />
					</FormGroup>
					<Button color="primary" onClick={this.mySubmitHandler} block>Ingresar</Button>
					<Button color="primary" onClick={this.props.viewRegister} block>Registrarse</Button>
					<Button color="primary" onClick={this.props.viewPassword} block>Recuperar Contraseña</Button>
				</Form>
			</Card>
		);
	}
}