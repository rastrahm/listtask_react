import React from 'react';
import { 
	Button, 
	Form, 
	FormGroup, 
	Label, 
	Input,
	Card,
	CardTitle
} from 'reactstrap';

export default class AppRegister extends React.Component {
    constructor(props) {
		super(props);
		this.state = {
			str_name: "",
			bol_active: 0,
			big_pro: 1,
			str_email: "",
			str_pass: "",
			close: props.close
		};
		this.handleChangeUser = this.handleChangeUser.bind(this);
		this.handleChangeMail = this.handleChangeMail.bind(this);
		this.handleChangePass = this.handleChangePass.bind(this);
	}

	mySubmitHandler = (event) => {
		event.preventDefault();
		var message = this.state;
		fetch(this.props.api + "/users/", {
			method: "POST",
			mode: "cors", 
			cache: "no-cache", 
			credentials: "same-origin", 
			headers: {
				"Content-Type": "application/json"
			},
			redirect: "follow", 
			referrerPolicy: "no-referrer",
			body: JSON.stringify(message)
		})
		.then(res => res.json())
		.then(
			(result) => {
				debugger;
				if (result.data[0].result === "t") {
					message = {
						id: result.data[0].id
					}
					fetch(this.props.api + "/listtaskmail/REGISTER/", {
						method: "POST",
						mode: "cors", 
						cache: "no-cache", 
						credentials: "same-origin", 
						headers: {
							"Content-Type": "application/json"
						},
						redirect: "follow", 
						referrerPolicy: "no-referrer",
						body: JSON.stringify(message)
					})
					.then(res => res.json())
					.then(
						(result) => {
							debugger;
							if (result.data[0].result === "t") {
								alert('Usuario incluido, recibira un E-Mail para la activación');
								this.state.close();
							} else {
								alert('Problema con carga de usuario, no se permiten E-Mail duplicados');
							};
					},
					// Note: it's important to handle errors here
					// instead of a catch() block so that we don't swallow
					// exceptions from actual bugs in components.
					(error) => {
						alert('Usuario invalido');
					})
					this.state.close();
				} else {
					alert('Problema con carga de usuario, no se permiten E-Mail duplicados');
				};
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
			alert('Usuario invalido');
        })
	}

	handleChangeUser(event) {
		this.setState({str_name: event.target.value});
	}

	handleChangeMail(event) {
		this.setState({str_email: event.target.value});
	}

	handleChangePass(event) {
		this.setState({str_pass: event.target.value});
	}

	render() {
		return (
			<Card body>
				<CardTitle><h3>Creación de Usuario</h3></CardTitle>
				<Form>
					<FormGroup>
						<Label for="user">Usuario</Label>
						<Input type="text" name="user" id="user" value={this.state.str_name}  onChange={this.handleChangeUser} placeholder="Introduzca su usuario" />
					</FormGroup>
					<FormGroup>
						<Label for="mail">Correo Electronico</Label>
						<Input type="email" name="email" id="email" value={this.state.str_email}  onChange={this.handleChangeMail} placeholder="Introduzca su email" />
					</FormGroup>
					<FormGroup>
						<Label for="pass">Password</Label>
						<Input type="password" name="pass" id="pass" value={this.state.str_pass} onChange={this.handleChangePass} placeholder="Introduzca su password" />
					</FormGroup>
					<Button color="primary" onClick={this.mySubmitHandler} block>Registrarse</Button>
				</Form>
			</Card>
		);
	}
}