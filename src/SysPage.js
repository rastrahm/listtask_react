import React from 'react';
import {
	Table
} from 'reactstrap';
import SkyLight from 'react-skylight';
import SysElementRow from './view/SysElementRow';
import SysElementTitle from './view/SysElementTitle';
import SysEditListTast from './view/SysEditListTask';
import SysEditParameters from './view/SysEditParameters';
import SysEditPreviews from './view/SysEditPreviews';
import SysEditUsers from './view/SysEditUsers';
import SysEditListTastJson from './view/SysEditListTaskJson';

export default class SysPage extends React.Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
			isOpen: false,
			isLink: "",
			isVisMod: false,
			actionType: "",
			jwt: props.jwt,
			data: []
		}
		this.viewEdit = this.viewEdit.bind(this);
		this.viewNew = this.viewNew.bind(this);
		this.viewDel = this.viewDel.bind(this);
		this.hideDialog = this.hideDialog.bind(this);
    }

    toggle() {
        this.setState({
			isOpen: !this.state.isOpen,
			isLink: "",
			data: []
		});
    }

    //Carga los elementos basicos del menú
    componentDidMount() {
		if ((this.props.link !== undefined) && (this.props.link !== "")) {
			fetch(window.location.protocol + "//" + window.location.hostname + "/functions/" + this.props.link)
			.then(res => res.json())
			.then(
				(result) => {
					this.setState({
						isLink: this.props.link,
						data: result.data[0]
					});
				},
				// Note: it's important to handle errors here
				// instead of a catch() block so that we don't swallow
				// exceptions from actual bugs in components.
				(error) => {
					this.setState({
						isLoaded: true,
						error
					});
			})
		} else {
			this.setState({data: [{str_name: ""}]});
		}
	}
	
	returnPage (){
		if ((this.props.data !== this.props.isLink) && (this.props.data !== undefined) && (this.props.data !== "")) {
			fetch(window.location.protocol + "//" + window.location.hostname + "/functions/" + this.props.route, {
				headers: {
					"jwt": this.state.jwt
				}
			})
			.then(res => res.json())
			.then(
				(result) => {
					this.setState({
						isLink: this.props.route,
						data: result.data
					});
					this.render();
				},
				// Note: it's important to handle errors here
				// instead of a catch() block so that we don't swallow
				// exceptions from actual bugs in components.
				(error) => {
					this.setState({
						isLoaded: true,
						error
					});
			})
		} else {
			this.setState({data: [{str_name: ""}]});
		}
	}

	viewNew(value) {
		let charge = {};
		for (let key in value) {
			if ((key.substr(0, 3) === 'big') || (key.substr(0, 3) === 'int') || (key.substr(0, 3) === 'flo')) {
				charge[key] = 0;
			} else if ((key.substr(0, 3) === 'str') || (key.substr(0, 3) === 'txt')) {
				charge[key] = '';
			} else if (key.substr(0, 3) === 'bol') {
				charge[key] = false;
			}
		}
		this.setState({
			actionType: 'new',
			isLink: value.route,
			values: charge,
			isVisMod: true
		});
		this.simpleDialog.show();
	}

	viewEdit(value) {
		this.setState({
			actionType: 'edit',
			isLink: value.route,
			values: value.data,
			isVisMod: true
		})
		this.simpleDialog.show();
	}

	viewDel(value) {
		alert(value);
	}

	hideDialog() {
		this.setState({
			isVisMod: false
		})
		this.simpleDialog.hide();
		this.returnPage();
	}

    render() {
		if ((this.props.data !== this.props.isLink) && (this.props.data !== undefined) && (this.props.data !== "")) {
			var formDialog = {
				width: '50%',
				'min-height': '50px',
				marginTop: '-300px',
				marginLeft: '-35%',
			};
			let viewOption = this.state.isVisMod;
			if (this.state.isVisMod) {
				//Recordar colocar keys
				switch (this.state.isLink) {
					case 'parameters':
						viewOption = <SysEditParameters key={this.state.isLink+this.state.values['big_id']} type={this.state.actionType} data={this.state.values} hide={this.hideDialog} jwt={this.state.jwt} route={this.state.isLink}></SysEditParameters>
						break;
					case 'previews':
						viewOption = <SysEditPreviews key={this.state.isLink+this.state.values['big_id']} type={this.state.actionType} data={this.state.values} hide={this.hideDialog} jwt={this.state.jwt} route={this.state.isLink}></SysEditPreviews>
						break;
					case 'users':
						viewOption = <SysEditUsers key={this.state.isLink+this.state.values['big_id']} type={this.state.actionType} data={this.state.values} hide={this.hideDialog} jwt={this.state.jwt} route={this.state.isLink}></SysEditUsers>
						break;
					case 'listtask':
						viewOption = <SysEditListTast key={this.state.isLink+this.state.values['big_id']} type={this.state.actionType} data={this.state.values} hide={this.hideDialog} jwt={this.state.jwt} route={this.state.isLink}></SysEditListTast>
						break;
					case 'listtaskjson':
						viewOption = <SysEditListTastJson key={this.state.isLink+this.state.values['big_id']} type={this.state.actionType} data={this.state.values} hide={this.hideDialog} jwt={this.state.jwt} route={this.state.isLink}></SysEditListTastJson>
						break;
					default:
						viewOption = <div></div>
						break;
				}
			} else {
				viewOption = <div></div>
			}
			return (
				<div>
					<Table>
						<thead>
							<SysElementTitle key={'T'+this.props.route} data={this.props.data} route={this.props.route} dictionary={this.props.dictionary} new={this.viewNew}></SysElementTitle>
						</thead>
						<tbody>
							<SysElementRow key={'E'+this.props.route} data={this.props.data} route={this.props.route} dictionary={this.props.dictionary} edit={this.viewEdit} del={this.viewDel}></SysElementRow>
						</tbody>
					</Table>
					<SkyLight dialogStyles={formDialog} hideOnOverlayClicked ref={ref => this.simpleDialog = ref} >
						{viewOption}
					</SkyLight>
				</div>
			)
			//<div>{this.props.data[0].big_id}</div>
		} else {
			return <div></div>
		}
    }
}