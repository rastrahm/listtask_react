import React from 'react';
import {
	td,
    Button,
    ButtonGroup
} from 'reactstrap';
import {AiOutlineEdit} from '@react-icons/all-files/ai/AiOutlineEdit'
import {AiOutlineMinus} from '@react-icons/all-files/ai/AiOutlineMinus'

export default class SysDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            route: props.route,
			data: props.data
		}
    }

    renderRow(value) {
        let result = [];
        let keys = Object.keys(value.data);
        for (let key in keys) {
            result.push(<td>{value.data[keys[key]]}</td>);
        }
        result.push(
            <td>
                <ButtonGroup>
                    <Button color="success" onClick={() => {value.edit(value)}}><AiOutlineEdit/></Button>
                    <Button color="danger" onClick={() => {value.del(value)}}><AiOutlineMinus/></Button>
                </ButtonGroup>
            </td>
        )
        return result;
    }

    render() {
        return (
            <this.renderRow key={this.props.data['big_id']} data={this.props.data} edit={this.props.edit} del={this.props.del} route={this.props.route}></this.renderRow>
        );
        
    }
}