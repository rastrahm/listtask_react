import React from 'react';
import {
    FormGroup, 
    Label
} from 'reactstrap';
import SysEditListTaskTask from './SysEditListTaskTask';

export default class SysEditListTaskGroup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: props.data.big_id,
            group: props.data.group,
            task: props.data.tasks
		}
        this.handleChangeGroup = this.handleChangeGroup.bind(this);
    }
    render() {
        return (
            <FormGroup>
                <Label for="group">Grupo</Label>
                <Input type="number" name="group" id="group" value={this.state.group}  />
                <SysEditListTaskTask></SysEditListTaskTask>
            </FormGroup>
        )
    };
}