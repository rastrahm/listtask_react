import React from 'react';
import {
	Form, 
    FormGroup, 
    Label
} from 'reactstrap';
import {AiOutlineCheck} from '@react-icons/all-files/ai/AiOutlineCheck'
import {AiOutlineClose} from '@react-icons/all-files/ai/AiOutlineClose'
import ReactJson from 'react-json-view';

export default class SysEditListTaskTask extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: props.data.big_id,
            group: props.data.group
		}
        this.handleChangeGroup = this.handleChangeGroup.bind(this);
    }
    render() {};
}