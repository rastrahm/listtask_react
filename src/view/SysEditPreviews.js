import React from 'react';
import {
	Button, 
    ButtonGroup,
    Form, 
    FormGroup, 
    Label, 
    Input
} from 'reactstrap';
import {AiOutlineCheck} from '@react-icons/all-files/ai/AiOutlineCheck'
import {AiOutlineClose} from '@react-icons/all-files/ai/AiOutlineClose'
import MDEditor from '@uiw/react-md-editor';

export default class SysEditPreviews extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            type: props.type,
            jwt: props.jwt,
            hide: props.hide,
            route: props.route,
			big_id: props.data.big_id,
            str_link: props.data.str_link,
            txt_title: props.data.txt_title,
            txt_article: props.data.txt_article
		}
        this.handleChangeLink = this.handleChangeLink.bind(this);
        this.handleChangeTitle = this.handleChangeTitle.bind(this);
        this.handleChangeArticle = this.handleChangeArticle.bind(this);
    }

    mySubmitHandler(values) {
		var message = {
            menu: values.route,
            big_id: values.big_id,
            str_link: values.str_link,
            txt_title: values.txt_title,
            txt_article: values.txt_article
        };
        let method = "PUT";
        if (values.type === 'new') {
            method = "POST";
        } 
		fetch(window.location.protocol + "//" + window.location.hostname + "/functions/" + values.route, {
			method: method,
			mode: "cors", 
			cache: "no-cache", 
			credentials: "same-origin", 
			headers: {
				"Content-Type": "application/json",
                "jwt": values.jwt
			},
			redirect: "follow", 
			referrerPolicy: "no-referrer",
			body: JSON.stringify(message)
		})
		.then(res => res.json())
		.then(
			(result) => {
                if (result.data[0].result === 't') {
                    debugger;
                    //hide showalert
                    values.hide();
                }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
			alert('Error de comunicación');
        })
    }

    handleChangeLink(event) {
        this.setState({str_link: event.target.value});
    }

    handleChangeTitle(value) {
        this.setState({txt_title: value});
    }

    handleChangeArticle(value) {
        this.setState({txt_article: value});
    }

    render() {
        return (
            <div>
                <Form>
                    {this.state.type === 'edit' ? 
                        <FormGroup>
                            <Label for="id">ID</Label><h3>{this.state.big_id}</h3>
                        </FormGroup>
                        :
                        <FormGroup>
                            <h3>Elemento Nuevo</h3>
                        </FormGroup>
                    }
					<FormGroup>
						<Label for="link">Enlace al Articulo</Label>
						<Input type="text" name="link" id="link" value={this.state.str_link} onChange={this.handleChangeLink} placeholder="Introduzca el nombre de la ruta" />
					</FormGroup>
                    <FormGroup>
                        <MDEditor key={'title'} value={this.state.txt_title} onChange={this.handleChangeTitle}/>
                    </FormGroup>
                    <FormGroup>
                        <MDEditor key={'article'} value={this.state.txt_article} onChange={this.handleChangeArticle}/>
                    </FormGroup>
                    <ButtonGroup style={{float: 'right'}}>
                        <Button color="primary" onClick={() => {this.mySubmitHandler(this.state)}}><AiOutlineCheck/></Button>
                        <Button color="danger" onClick={this.props.hide}><AiOutlineClose /></Button>
                    </ButtonGroup>
                </Form>
            </div>
        )
    }
}
/*<FormGroup>
						<Label for="article">Cuerpo del Articulo</Label>
						<Input type="textarea" name="article" id="article" value={this.state.txt_article} onChange={this.handleChangeArticle} placeholder="Introduzca el cuerpo del articulo" />
					</FormGroup>
                    <FormGroup>
						<Label for="title">Titulo del Articulo</Label>
						<Input type="textarea" name="title" id="title" value={this.state.txt_title} onChange={this.handleChangeTitle} placeholder="Introduzca el titulo del articulo" />
					</FormGroup>
                    //No esta claro el ejemplo para capturar los eventos
                    <FormGroup>
                        <TextareaMarkdownEditor value={this.state.txt_article}  onKeyPress={this.handleChangeArticle}/>
                    </FormGroup>
                    */