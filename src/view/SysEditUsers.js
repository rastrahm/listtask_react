import React from 'react';
import {
	Button, 
    ButtonGroup,
    Form, 
    FormGroup, 
    Label, 
    Input
} from 'reactstrap';
import {AiOutlineCheck} from '@react-icons/all-files/ai/AiOutlineCheck'
import {AiOutlineClose} from '@react-icons/all-files/ai/AiOutlineClose'

export default class SysEditUsers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            type: props.type,
            jwt: props.jwt,
            hide: props.hide,
            route: props.route,
			big_id: props.data.big_id,
            str_name: props.data.str_name,
            bol_active: props.data.bol_active ? "true" : "false",
            int_pro: props.data.big_profile,
            str_pro: props.data.str_profile_name,
            str_email: props.data.str_email,
            profiles: []
		}
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeActive = this.handleChangeActive.bind(this);
        this.handleChangePro = this.handleChangePro.bind(this);
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangePass = this.handleChangePass.bind(this);
    }

    componentDidMount() {
        fetch(window.location.protocol + "//" + window.location.hostname + "/functions/profiles", {
            headers:{
                'jwt': this.state.jwt
            }
        })
        .then(res => res.json())
        .then(
            (result) => {
                this.setState({
                    profiles: result.data
                });
            },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                });
            }
        )
    }

    mySubmitHandler(values) {
        let bol_active = 0;
        if ((values.bol_active === true) || (values.bol_active === 'true')) {
            bol_active = 1;
        }
        let message = {
            "menu": values.route,
            "big_id" : values.big_id,
            "str_name" : values.str_name,
            "bol_active" : bol_active,
            "big_pro": values.int_pro,
            "str_email": values.str_email,
        }
        let method = "PUT";
        if (values.type === 'new') {
            method = "POST";
            message['str_pass'] = values.str_pass;
        } 
		fetch(window.location.protocol + "//" + window.location.hostname + "/functions/" + values.route, {
			method: method,
			mode: "cors", 
			cache: "no-cache", 
			credentials: "same-origin", 
			headers: {
				"Content-Type": "application/json",
                "jwt": values.jwt
			},
			redirect: "follow", 
			referrerPolicy: "no-referrer",
			body: JSON.stringify(message)
		})
		.then(res => res.json())
		.then(
			(result) => {
                if (result.data[0].result === 't') {
                    debugger;
                    //hide showalert
                    values.hide();
                } else {
                    alert(result.data);
                }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
			alert('Error de comunicación');
        })
    }

    handleChangeName(event) {
        this.setState({str_name: event.target.value});
    }

    handleChangeActive(event) {
        this.setState({bol_active: event});
    }

    handleChangePro(event) {
        this.setState({int_pro: event});
    }

    handleChangeEmail(event) {
        this.setState({str_email: event.target.value});
    }

    handleChangePass(event) {
        this.setState({str_pass: event.target.value});
    }

    render() {
        return (
            <div>
                <Form>
                    {this.state.type === 'edit' ? 
                        <FormGroup>
                            <Label for="id">ID</Label><h3>{this.state.big_id}</h3>
                        </FormGroup>
                        :
                        <FormGroup>
                            <h3>Elemento Nuevo</h3>
                        </FormGroup>
                    }
					<FormGroup>
						<Label for="name">Nombre del usuario</Label>
						<Input type="text" name="name" id="name" value={this.state.str_name} onChange={this.handleChangeName} placeholder="Introduzca el nombre del paramentro" />
					</FormGroup>
                    <FormGroup>
						<Label for="active">Usuario Activo</Label>
						<Input  style={{marginLeft: '5px'}} type="checkbox" name="active" id="active" value={this.state.bol_active} onChange={this.handleChangeActive} defaultChecked={this.state.bol_active} />{' '}
					</FormGroup>
                    <FormGroup>
						<Label for="pro">Perfil del Usuario</Label>
						<Input type="select" name="pro" id="pro" value={this.state.int_pro} onChange={(event) => {this.handleChangePro(event.target.value)}} defaultValue={this.state.int_pro}>
                            {this.state.profiles.map((value) => {
                                return <option key={value.big_id} value={value.big_id}>{value.str_name}</option>;
                            })}
                        </Input>
					</FormGroup>
                    <FormGroup>
						<Label for="email">Correo Electronico</Label>
						<Input type="email" name="email" id="email" value={this.state.str_email} onChange={this.handleChangeEmail} placeholder="Introduzca el correo electronico" />
					</FormGroup>
                    {this.state.type === 'new' ?
                        <FormGroup>
                            <Label for="pass">Clave de usuario</Label>
                            <Input type="password" name="pass" id="pass" value={this.state.str_pass} onChange={this.handleChangePass} placeholder="Introduzca el password" />
                        </FormGroup>
                        :
                        <div></div>
                    }
                    <ButtonGroup style={{float: 'right'}}>
                        <Button color="primary" onClick={() => {this.mySubmitHandler(this.state)}}><AiOutlineCheck/></Button>
                        <Button color="danger" onClick={this.props.hide}><AiOutlineClose /></Button>
                    </ButtonGroup>
                </Form>
            </div>
        )
    }
}