import React from 'react';
import {
	tr
} from 'reactstrap';
import SysDetail from './SysDetail';

export default class SysElementRow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
			data: props.data
		}
    }

    render() {
        return(
            this.state.data.map((element) => {
                return (
                    <tr>
                        <SysDetail key={element['big_id']} data={element} dictionary={this.props.dictionary} edit={this.props.edit} del={this.props.del} route={this.props.route}></SysDetail>
                    </tr>
                )
            })
        )
    }
}