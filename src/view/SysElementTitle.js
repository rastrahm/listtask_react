import React from 'react';
import {
	tr,
    th,
    Button
} from 'reactstrap';
import {AiOutlinePlus} from '@react-icons/all-files/ai/AiOutlinePlus'

export default class SysElementTitle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            route: props.route,
			data: props.data
		}
    }

    RenderTitle(value) {
        let result = [];
        let keys = Object.keys(value.data[0]);
        for (let key in keys) {
            let track = value.dictionary.find(element => element['str_name'] === keys[key]);
            track = track['str_value'];
            if (track){
                result.push(<th>{track}</th>);
            } else {
                result.push(<th>{keys[key]}</th>);
            }
        }
        result.push(<th style={{width: '100px'}}><Button color="primary" size="lg" style={{width: '80px'}} onClick={() => {value.new(value)}}><AiOutlinePlus/></Button></th>);
        return result;
    }

    render() {
        return(
            <tr>
                <this.RenderTitle data={this.props.data} dictionary={this.props.dictionary} new={this.props.new} route={this.props.route}></this.RenderTitle>
            </tr>
        )
    }
}